# NLP for Humanists: An Introduction to Key Concepts and Workflows [Natasha/Toma]

## What you'll learn
inequalities in scholarship created by access to technology
overcoming barriers to research by creating the necessary tools.

## Learning outcomes

At the end of this course, students will be able to:
- Evaluate common research uses of text analysis and natural language processing in the digital humanities
- Assess how their research would benefit from large-scale text analysis.
- Identify where pattern-matching would be the most useful solution versus when a statistical language model is necessary.
- Assess fine-tuning is required for research. (Early Modern place name example)



