5. Preparing Data for New Language Models in spaCy - Andy & Quinn
    (this should cover Cadet, Inception etc.)


## Learning outcomes

At the end of this course, students will be able to:
- Generate and customize a spaCy language object using Cadet
- Bulk-annotate non-ambiguous terms using Cadet
- Use inception and recommenders to quickly annotate texts
- Export annotations for use in model training


Terminology
- Bulk-annotation
- Recommenders
- Manual annotation

### Using Cadet videos

- Cadet the notebook (CM)
- Tokenization (CM)

### Overview of Inception videos


## Exporting data 
- Universal Dependencies, when they're right, what they don't capure and how to create your own data
- export data formats (conllu vs conll 2002 (ner))


Would want WebAnno to DocBin converter
or [UIMA CAS JSON to Doc](https://thinkinfi.com/prepare-training-data-and-train-custom-ner-using-spacy-python/)