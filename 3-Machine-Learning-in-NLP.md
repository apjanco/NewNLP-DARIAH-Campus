# Machine Learning in NLP: An Introduction [David]

Models, training, neural networks but very basic and easy to understand for humanists, no maths, just conceptual description and why it can be useful
Ethics of Machine Learning & NLP; Stochastic Parrots


## Learning outcomes

At the end of this course, students will:
- Have a basic conceptual understanding of neural networks
- Understand how linguistic data is used to a train a spaCy statistical language model (features provide model with information to improve predictions)
- The capabilities and limitations of NLP 🦜 (predictive, machine errors, best to augment and scale existing research practices/not replace them)


## Sequence Models vs. Transformers
- 
- Masking, bidirectional
- Contextual embeddings
- Attention   
insert [Embeddings, Do You Need Them?](https://new-languages-for-nlp.github.io/course-materials/w2/embeddings.html)

Word/token embeddings?