# Practical Introduction to Spacy for Humanists [Andy/Nick]

How does a scholar use NLP as a research tool? What becomes possible? How natural language processing can lead to new knowledge.

## Learning outcomes

At the end of this course, students will be able to:
- Match patterns with spaCy matcher
- named entity recognition (people, places, orgs)
- named entity linking (wikidata, kb)
- span labeling and categorization (Litbank)

## Text as a sequence of charachters

...

## Tokenization
- What is tokenization good for?
[work from CM](https://new-languages-for-nlp.github.io/course-materials/w1/tokenization.html)

### tokenization rules and exceptions 

## Token Attributes
build from https://spacy.io/api/token#attributes

## spaCy Matcher
build from spaCy course, but focus on DH research tasks https://course.spacy.io/en/chapter1

## Named entities 

## Entity Linking 

## Span Labeling and Catgorization


## Dependencies

## Nick's Confucius example 