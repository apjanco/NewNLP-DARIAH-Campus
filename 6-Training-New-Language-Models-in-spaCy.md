### Training Models with Inception Data video (Quinn/Andy)


At the end of this course, students will be able to:
- Project file
- Config file
- New Language Project file

## LitBank example (model training)
[insert Practical Introduction CM](https://new-languages-for-nlp.github.io/course-materials/w2/practical-intro/Practical%20Introduction%20to%20Model%20Training.html
)  
[insert Practical Introduction video - Quinn]()


## New Language Project file
- [notebook](https://new-languages-for-nlp.github.io/course-materials/w2/using-inception-data/New%20Language%20Training.html)